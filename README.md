# Learning Git

A simple slideshow I wrote for my team. The objectives are:
1. Get set up with git
1. Learn to clone
1. Learn to push changes to files

# Technologies
- __knitr__ Used to produce a slidy_presentation slideshow from __git and gitlab.rmd__.

# Installing and Running

Cloning is recommended, though you may download this repo if you wish.

Ensure knitr is installed.

## Creating the Slideshow

Open __git and gitlab.rmd__

Select the knit button. This will produce the slideshow and save it under the name __git and gitlab.html__ in your local project directory.

When you have __git and gitlab.html__ saved, you do not have to knit it again unless you make changes to it. 

